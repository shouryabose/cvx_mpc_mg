clear all;
close all;

bus_data_format;

MTRelCost = 0.05;

tmax = 1;
twindow = 10;
Result = [];

for t = 1:tmax
    cvx_begin
    variable LineRealPower(twindow*nbr_dir);
    variable LineCurrentSq(twindow*nbr_dir) nonnegative;
    variable NodeVoltageSq(twindow*nbus) nonnegative;
    variable NodeRealPower(twindow*nbus);
    variable ESSPch(twindow*nESS);
    variable ESSPdis(twindow*nESS);
    variable SoC(twindow*nESS);
    variable FuelState(twindow*nMT) nonnegative;
    variable RESCur(twindow*nRES);
    variable Pickup(twindow*nload);
    variable tmpObj(twindow);
    variable Objective;
    maximize Objective;
    % set bounds
    0 <= ESSPch <= MaxChargeRealPower;
    0 <= ESSPdis <= MaxDischargeRealPower;
    MinSoC <= SoC <= MaxSoC;
    0 <= RESCur <= 1;
    0 <= Pickup <= 1;
    0.95 <= NodeVoltageSq <= 1.05;
    -0.5 <= LineRealPower <= 0.5;
    0 <= LineCurrentSq <= 8;
    for t_mpc = 1:twindow
        muline = (t_mpc-1)*nbr_dir;
        mnode = (t_mpc-1)*nbus;
        mESS = (t_mpc-1)*nESS;
        mRES = (t_mpc-1)*nRES;
        mload = (t_mpc-1)*nload;
        mMT = (t_mpc-1)*nMT;
        % DistFlow first set
        for c1 = 1:nbus
            nbd_downstream = findri2(BusBranch,c1); % Downstream neighborhood of node c1
            nbd_upstream = findri3(BusBranch,c1); % Upstream neighborhood of node c1
            if isempty(nbd_downstream)
                resu = zeros(numel(nbd_upstream),1);
                for c2 = 1:numel(nbd_upstream)
                    busu = BusBranch(nbd_upstream(c2),:);
                    resu(c2) = full(RBUS(busu(1),busu(2)));
                end
                -sum(LineRealPower(muline+(nbd_upstream)) - resu.*LineCurrentSq(muline+(nbd_upstream))) == NodeRealPower(mnode+c1);
            else
                if isempty(nbd_upstream)
                    sum(LineRealPower(muline+(nbd_downstream)))  == NodeRealPower(mnode+c1);
                else
                    resu = zeros(numel(nbd_upstream),1);
                    for c2 = 1:numel(nbd_upstream)
                        busu = BusBranch(nbd_upstream(c2),:);
                        resu(c2) = full(RBUS(busu(1),busu(2)));
                    end
                    sum(LineRealPower(muline+(nbd_downstream))) - sum(LineRealPower(muline+(nbd_upstream)) - resu.*LineCurrentSq(muline+(nbd_upstream)))  == NodeRealPower(mnode+c1);
                end
            end
        end
        % distflow second set
        for c1 = 1:nbr_dir
            cbus = BusBranch(c1,:);
            res = full(RBUS(cbus(1),cbus(2)));
            NodeVoltageSq(mnode+cbus(2)) == NodeVoltageSq(mnode+cbus(1)) - 2*res*LineRealPower(muline+c1) + (res^2)*LineCurrentSq(muline+c1);
        end
        % distflow inequalities
        for c1 = 1:nbr_dir
            cur_branch = BusBranch(c1,:);
            norm([2*LineRealPower(muline+c1),LineCurrentSq(muline+c1)-NodeVoltageSq(mnode+cur_branch(1))],2) <= (LineCurrentSq(muline+c1)+NodeVoltageSq(mnode+cur_branch(1)));
        end
        % ESS constraints
        for c1 = 1:nESS
            -ESSPch(mESS+c1) + ESSPdis(mESS+c1) == NodeRealPower(mnode+ESSbus(c1));
        end
        % MT power bounds
        MTLowerRealLimit <= NodeRealPower(mnode+(MTbus)) <= MTUpperRealLimit;
        % RES power
        rng(t_mpc+t^2*t_mpc);
        rRealPower = normrnd(meanPower,varPower);
        RESRealPower = min(max([rRealPower,0]),InverterNameplate);
        for c1 = 1:nRES
            NodeRealPower(mnode+RESbus(c1)) == RESCur(mRES+c1)*RESRealPower;
        end
        % Load time decoupled constraints
        for c1 = 1:nload
            NodeRealPower(mnode+loadbus(c1)) == -Pickup(mload+c1)*(RealLoad(c1)+max([0,normrnd(0,0.05)]));
            %NodeRealPower(mnode+loadbus(c1)) == -Pickup(mload+c1)*(RealLoad(c1));
        end
        % time couple constraints
        if t_mpc == 1 % First step of MPC window
            if t == 1 % constraints that are different for first timestep
                for c1 = 1:nMT
                    FuelState(mMT+c1) == FuelTotal(c1) - efficiencyFactor*NodeRealPower(mnode+MTbus(c1));
                    NodeRealPower(mnode+MTbus(c1)) <= RampUp;
                end
                % Inital Battery SoC balance
                for c1 = 1:nESS
                    SoC(mESS+c1) - InitSoC(1,c1) - chargeEfficiency*ESSPch(mESS+c1) + dischargeEfficiency*ESSPdis(mESS+c1) == 0;
                end
            else
                for c1 = 1:nMT
                    FuelState(mMT+c1) == MPCsol.FuelState(c1) - efficiencyFactor*NodeRealPower(mnode+MTbus(c1));
                    RampDown <= NodeRealPower(mnode+MTbus(c1)) - MPCsol.NodeRealPower(MTbus(c1)) <= RampUp;
                end
                % Battery SoC balance
                for c1 = 1:nESS
                    SoC(mESS+c1) - MPCsol.SoC(c1) - chargeEfficiency*ESSPch(mESS+c1) + dischargeEfficiency*ESSPdis(mESS+c1) == 0;
                end
                % Load Satisfaction should be monotonically increasing
                for c1 = 1:nload
                    Pickup(mload+c1) - MPCsol.Pickup(c1) >= 0;
                end
            end
        else
            % MT fuel balance, Ramp up and Ramp down constraints
            for c1 = 1:nMT
                FuelState(mMT+c1) == FuelState((mMT-nMT)+c1) - efficiencyFactor*NodeRealPower(mnode+MTbus(c1));
                RampDown <= NodeRealPower(mnode+MTbus(c1)) - NodeRealPower((mnode-nbus)+MTbus(c1)) <= RampUp;
            end
            % Battery SoC balance
            for c1 = 1:nESS
                SoC(mESS+c1) - SoC((mESS-nESS)+c1) - chargeEfficiency*ESSPch(mESS+c1) + dischargeEfficiency*ESSPdis(mESS+c1) == 0;
            end
            % Load Satisfaction should be monotonically increasing
            for c1 = 1:nload
                Pickup(mload+c1) - Pickup((mload-nload)+c1) >= 0;
            end
        end
        %tmpObj(t_mpc) == -sum(NodeRealPower(mnode+(loadbus))) - MTRelCost*sum(NodeRealPower(mnode+(MTbus)));
        tmpObj(t_mpc) == sum(Pickup(mload+(1:nload))) - MTRelCost*sum(NodeRealPower(mnode+(MTbus)));
    end
    Objective == sum(tmpObj);
    cvx_end
    MPCsol.LineRealPower = LineRealPower(1:nbr_dir);
    MPCsol.LineCurrentSq = LineCurrentSq(1:nbr_dir);
    MPCsol.NodeVoltageSq = NodeVoltageSq(1:nbus);
    MPCsol.NodeRealPower = NodeRealPower(1:nbus);
    MPCsol.ESSPch = ESSPch(1:nESS);
    MPCsol.ESSPdis = ESSPdis(1:nESS);
    MPCsol.SoC = SoC(1:nESS);
    MPCsol.FuelState = FuelState(1:nMT);
    MPCsol.RESCur = RESCur(1:nRES);
    MPCsol.Pickup = Pickup(1:nload);
    MPCsol.Objective = Objective;
    Result{t} = MPCsol;
end

Result = [];

for i = 1:twindow
    mline = (i-1)*nbr_dir;
    mnode = (i-1)*nbus;
    mESS = (i-1)*nESS;
    mMT = (i-1)*nMT;
    mRES = (i-1)*nRES;
    mload = (i-1)*nload;
    Result{i}.LineRealPower = LineRealPower(mline+(1:nbr_dir));
    Result{i}.LineCurrentSq = LineCurrentSq(mline+(1:nbr_dir));
    Result{i}.NodeVoltageSq = NodeVoltageSq(mnode+(1:nbus));
    Result{i}.NodeRealPower = NodeRealPower(mnode+(1:nbus));
    Result{i}.ESSPch = ESSPch(mESS+(1:nESS));
    Result{i}.ESSPdis = ESSPdis(mESS+(1:nESS));
    Result{i}.SoC = SoC(mESS+(1:nESS));
    Result{i}.FuelState = FuelState(mMT+(1:nMT));
    Result{i}.RESCur = RESCur(mRES+(1:nRES));
    Result{i}.Pickup = Pickup(mload+(1:nload));
    %Result{i}.Objective = Objective;
end

plotscript_CVX
