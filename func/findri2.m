function rowindex = findri2(X,a)

% find which row of 2 column matrix X has column1 value a
% else return empty variable

[sz1,sz2] = size(X);

assert(sz2==2,'Matrix X doesnt have 2 columns.');

rowindex = [];

for i = 1:sz1
    if X(i,1) == a
        rowindex = [rowindex;i];
    end
end

end