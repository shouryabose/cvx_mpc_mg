function rowindex = findri3(X,b)

% find which row of 2 column matrix X has column2 value b
% else return empty variable

[sz1,sz2] = size(X);

assert(sz2==2,'Matrix X doesnt have 2 columns.');

rowindex = [];

for i = 1:sz1
    if X(i,2) == b
        rowindex = [rowindex;i];
    end
end

end