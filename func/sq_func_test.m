clear all;
close all;

[M,C] = sq_func_approx(6,0.5);
f = @(x) 0.*x;
for i = 1:numel(M)
    f = @(x) max([f(x);M*x+C]);
end

xp = -1.5:0.001:1.5;
yp = xp.^2;
yp2 = f(xp);

F = figure; hold on; box on;
plot(xp,yp,'linewidth',2);
plot(xp,yp2,'linewidth',2);