% this script is to calculate the differentials of all power terms

iPbus = zeros(1,nbus);
iQbus = zeros(1,nbus);
iV2bus = zeros(1,nbus);
iPbr = zeros(1,nbr_dir);
iQbr = zeros(1,nbr_dir);
iLbr = zeros(1,nbr_dir);

cur.Pbr = Result{1}.Pline;
cur.Qbr = Result{1}.Qline;
cur.V2bus = Result{1}.V2node;
cur.Lbr = Result{1}.I2line;

% Extract voltages, currents and line values

% set up indices

count = 1;

for i = 1:nbus
    iPbus(i) = count;
    count = count + 1;
end

for i = 1:nbus
    iQbus(i) = count;
    count = count + 1;
end

for i = 1:nbus
    iV2bus(i) = count;
    count = count + 1;
end

for i = 1:nbr_dir
    iPbr(i) = count;
    count = count + 1;
end

for i = 1:nbr_dir
    iQbr(i) = count;
    count = count + 1;
end

for i = 1:nbr_dir
    iLbr(i) = count;
    count = count + 1;
end

count = count - 1; % reverse extra count add

delX = zeros(count,1);
A = [];

% set up the A matrix

for c1 = 1:nbus
    nbd_downstream = findri2(BusBranch,c1); % Downstream neighborhood of node c1
    nbd_upstream = findri3(BusBranch,c1); % Upstream neighborhood of node c1
    curArow = zeros(1,count);
    curArow(1,iPbus(c1)) = 1;
    for j = 1:numel(nbd_downstream)
        curArow(iPbr(nbd_downstream(j))) = -1;
    end
    for j = 1:numel(nbd_upstream)
        curArow(iPbr(nbd_upstream(j))) = -1;
        curArow(iLbr(nbd_upstream(j))) = RBUS(nbd_upstream(j));
    end
%     A
%     curArow
    A = [A;curArow];
end

for c1 = 1:nbus
    nbd_downstream = findri2(BusBranch,c1); % Downstream neighborhood of node c1
    nbd_upstream = findri3(BusBranch,c1); % Upstream neighborhood of node c1
    curArow = zeros(1,count);
    curArow(1,iQbus(c1)) = 1;
    for j = 1:numel(nbd_downstream)
        curArow(iQbr(nbd_downstream(j))) = -1;
    end
    for j = 1:numel(nbd_upstream)
        curArow(iQbr(nbd_upstream(j))) = -1;
        curArow(iLbr(nbd_upstream(j))) = XBUS(nbd_upstream(j));
    end
    A = [A;curArow];
end

for c1 = 1:nbr_dir
    c_br = BusBranch(c1,:);
    sendEnd = c_br(1);
    recEnd = c_br(2);
    curArow = zeros(1,count);
    curArow(iV2bus(recEnd)) = 1;
    curArow(iV2bus(sendEnd)) = -1;
    curArow(iPbr(c1)) = 2*RBUS(c1);
    curArow(iQbr(c1)) = 2*XBUS(c1);
    curArow(iLbr(c1)) = -(RBUS(c1)^2 + XBUS(c1)^2);
    A = [A;curArow];
end

for c1 = 1:nbr_dir
    c_br = BusBranch(c1,:);
    sendEnd = c_br(1);
    recEnd = c_br(2);
    curArow = zeros(1,count);
    curArow(iLbr(c1)) = cur.V2bus(sendEnd);
    curArow(iV2bus(sendEnd)) = cur.Lbr(c1);
    curArow(iPbr(c1)) = -2*cur.Pbr(c1);
    curArow(iQbr(c1)) = -2*cur.Qbr(c1);
    A = [A;curArow];
end

[sz1,sz2] = size(A);

assert(sz1==2*(nbus+nbr_dir) && sz2==3*(nbus+nbr_dir),'Wrong size of matrix A');

NS_A = null(A);
delX = NS_A(:,1);