function [Rgrad_a, Cgrad_B, c] = CalculateLinearParams(act,state)

global Pload Qload PMT QMT PRES QRES PESSCh PESSDis QESS sz_mean sz_var H;
global gamma;
global RealLoad ReactLoad loadbus MTbus ESSbus RESbus;
global efficiencyFactor MTUpperRealLimit MTLowerRealLimit MTUpperReactLimit MTLowerReactLimit FuelTotal;
global count2 SOCIndex FuelIndex RealD ReactD RESIndex;
global meanPower InverterNameplate;
global MaxChargeRealPower MaxDischargeRealPower dischargeEfficiency chargeEfficiency MaxSoC MinSoC MaxESSReactPower MinESSReactPower;


nload = numel(loadbus);
nMT = numel(MTbus);
nRES = numel(RESbus);
nESS = numel(ESSbus);
nbus = nload+nMT+nRES+nESS;

Rgrad_a = zeros(1,numel(act));
Cgrad_B = [];
c = [];
RealDemand = -RealLoad(loadbus);
ReactDemand = -ReactLoad(loadbus);

% calculate $a$
for h = 1:H
    for i = 1:nload
        Rgrad_a(Pload((h-1)*nload+i)) = (gamma^(h-1))/(RealDemand(i));
    end
    for i = 1:nMT
        Rgrad_a(PMT((h-1)*nMT+i)) = (gamma^(h-1))/(MTLowerRealLimit-MTUpperRealLimit);
    end
end

std_row = zeros(1,numel(act));

% real load constraints

for i = 1:nload
    a = std_row;
    constr = 0;
    for h = 1:H
        plevel = act(Pload((h-1)*nload+i));
        constr = constr + gamma^(h-1)*plevel;
        a(Pload((h-1)*nload+i)) = gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

for i = 1:nload
    a = std_row;
    constr = 0;
    for h = 1:H
        plevel = act(Pload((h-1)*nload+i));
        constr = constr + gamma^(h-1)*(-plevel+RealDemand(i));
        a(Pload((h-1)*nload+i)) = -gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

% react load constraints

for i = 1:nload
    a = std_row;
    constr = 0;
    for h = 1:H
        qlevel = act(Qload((h-1)*nload+i));
        constr = constr + gamma^(h-1)*qlevel;
        a(Qload((h-1)*nload+i)) = gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

for i = 1:nload
    a = std_row;
    constr = 0;
    for h = 1:H
        qlevel = act(Qload((h-1)*nload+i));
        constr = constr + gamma^(h-1)*(-qlevel+ReactDemand(i));
        a(Qload((h-1)*nload+i)) = -gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

% MT Real constraints

for i = 1:nMT
    a = std_row;
    constr = 0;
    for h = 1:H
        plevel = act(PMT((h-1)*nMT+i));
        constr = constr + gamma^(h-1)*(plevel-MTUpperRealLimit);
        a(PMT((h-1)*nMT+i)) = gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

for i = 1:nMT
    a = std_row;
    constr = 0;
    for h = 1:H
        plevel = act(PMT((h-1)*nMT+i));
        constr = constr + gamma^(h-1)*(-plevel+MTLowerRealLimit);
        a(PMT((h-1)*nMT+i)) = -gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

% MT Reactive constraints

for i = 1:nMT
    a = std_row;
    constr = 0;
    for h = 1:H
        qlevel = act(QMT((h-1)*nMT+i));
        constr = constr + gamma^(h-1)*(qlevel-MTUpperReactLimit);
        a(QMT((h-1)*nMT+i)) = gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

for i = 1:nMT
    a = std_row;
    constr = 0;
    for h = 1:H
        qlevel = act(QMT((h-1)*nMT+i));
        constr = constr + gamma^(h-1)*(-qlevel+MTUpperReactLimit);
        a(QMT((h-1)*nMT+i)) = -gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

% RES Constraint

for i = 1:nRES
    a = std_row;
    constr = 0;
    for h = 1:H
        plevel = act(PRES((h-1)*nRES+i));
        constr = constr + gamma^(h-1)*(plevel-state(RESIndex((h-1)*nRES+i)));
        a(PRES((h-1)*nRES+i)) = gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

for i = 1:nRES
    a = std_row;
    constr = 0;
    for h = 1:H
        plevel = act(PRES((h-1)*nRES+i));
        constr = constr + gamma^(h-1)*(-plevel);
        a(PRES((h-1)*nRES+i)) = -gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

% RES Reactive constraint

for i = 1:nRES
    a = std_row;
    constr = 0;
    for h = 1:H
        qlevel = act(QRES((h-1)*nRES+i));
        constr = constr + gamma^(h-1)*(qlevel-(InverterNameplate-state(RESIndex((h-1)*nRES+i))));
        a(QRES((h-1)*nRES+i)) = gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

for i = 1:nRES
    a = std_row;
    constr = 0;
    for h = 1:H
        qlevel = act(QRES((h-1)*nRES+i));
        constr = constr + gamma^(h-1)*(-qlevel+(InverterNameplate-state(RESIndex((h-1)*nRES+i))));
        a(QRES((h-1)*nRES+i)) = -gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

% ESS Charge constraint

for i = 1:nESS
    a = std_row;
    constr = 0;
    for h = 1:H
        plevel = act(PESSCh((h-1)*nESS+i));
        constr = constr + gamma^(h-1)*(plevel-MaxChargeRealPower);
        a(PESSCh((h-1)*nESS+i)) = gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

for i = 1:nESS
    a = std_row;
    constr = 0;
    for h = 1:H
        plevel = act(PESSCh((h-1)*nESS+i));
        constr = constr + gamma^(h-1)*(-plevel);
        a(PESSCh((h-1)*nESS+i)) = -gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

% ESS Discharge Constraint

for i = 1:nESS
    a = std_row;
    constr = 0;
    for h = 1:H
        plevel = act(PESSDis((h-1)*nESS+i));
        constr = constr + gamma^(h-1)*(plevel-MaxDischargeRealPower);
        a(PESSDis((h-1)*nESS+i)) = gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

for i = 1:nESS
    a = std_row;
    constr = 0;
    for h = 1:H
        plevel = act(PESSDis((h-1)*nESS+i));
        constr = constr + gamma^(h-1)*(-plevel);
        a(PESSDis((h-1)*nESS+i)) = -gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

% ESS Discharge Constraint

for i = 1:nESS
    a = std_row;
    constr = 0;
    for h = 1:H
        qlevel = act(QESS((h-1)*nESS+i));
        constr = constr + gamma^(h-1)*(qlevel-MaxESSReactPower);
        a(QESS((h-1)*nESS+i)) = gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

for i = 1:nESS
    a = std_row;
    constr = 0;
    for h = 1:H
        qlevel = act(QESS((h-1)*nESS+i));
        constr = constr + gamma^(h-1)*(-qlevel+MinESSReactPower);
        a(QESS((h-1)*nESS+i)) = -gamma^(h-1);
    end
    c = [c;constr];
    Cgrad_B = [Cgrad_B;a];
end

end


