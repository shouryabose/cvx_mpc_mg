mpc = loadcase('case12da');

global H loadbus MTbus ESSbus RESbus;
global RBUS XBUS BusBranch;
global M C;
global Pload Qload PMT QMT PRES QRES PESSCh PESSDis QESS sz_mean sz_var;
global efficiencyFactor MTUpperRealLimit MTLowerRealLimit MTUpperReactLimit MTLowerReactLimit FuelTotal;
global meanPower InverterNameplate;
global MaxChargeRealPower MaxDischargeRealPower dischargeEfficiency chargeEfficiency MaxSoC MinSoC MaxESSReactPower MinESSReactPower;
global Vmin Vmax;
global RealLoad ReactLoad;

RBUS = mpc.branch(:,3);
XBUS = mpc.branch(:,4);

% type modification
% 1 - MT
% 2 - ESS
% 3 - RES
% 4 - Load

nbus = numel(mpc.bus(:,1));

bustype = mpc.bus(:,2);
bustype = 4*ones(size(bustype));
bustype([1]) = 1;
bustype([4]) = 2;
bustype([10]) = 3;
busdemand = mpc.bus(:,3) + 1i*mpc.bus(:,4);
RealLoad = real(busdemand);
ReactLoad = imag(busdemand);

loadbus = find(bustype==4); nload = numel(loadbus);
MTbus = find(bustype==1); nMT = numel(MTbus);
ESSbus = find(bustype==2); nESS = numel(ESSbus);
RESbus = find(bustype==3); nRES = numel(RESbus);

% Branches
[sz1,sz2] = size(mpc.branch);
BusBranch = double(mpc.branch(:,1:2));
nbr_dir = sz1;

% microturbine Parameters
efficiencyFactor = 0.8;
FuelTotal = 2;
RampUp = 0.15;
RampDown = -0.1;
MTUpperRealLimit = 0.3;
MTLowerRealLimit = 0;
MTUpperReactLimit = 0.3;
MTLowerReactLimit = -0.3;
UnitCost = 2*ones(nMT,1);

% battery Parameters
dischargeEfficiency = 0.8;
chargeEfficiency = 1/0.8;
MaxSoC = 5;
MinSoC = 1;
InitSoC = 1.75;
MaxDischargeRealPower = 0.15;
MaxChargeRealPower = 0.2;
MinDischargeRealPower = 0;
MinChargeRealPower = 0;
MaxESSReactPower = 0.2;
MinESSReactPower = -0.2;

% RES
meanPower = 0.07;
varPower = 0.05; 
InverterNameplate = 0.2;

% Load
priority = ones(nload,1);
RealLoad(1) = RealLoad(2);

% Voltage
Vmin = 0.95;
Vmax = 1.05;

% Tolerance factor for load restoration monotonicity
BalanceFac = 0.15;

ReactLoad(1) = ReactLoad(2);

% plotscript