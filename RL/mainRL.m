% mainRL script

clear all;
close all;

%% Define parameters

global H gamma;
H = 3; % time horizon
gamma = 0.9;
nEpisodes = 20;
lenEpisode = 5;
avEvent = 2;
MonteCarloParam = 20;

bus_data_format;

indexVars;

inputSize = nESS + nMT + H*(2*nload + nRES);
outputSize = count;

%% TEMPORARY
RealLoad = 0.1*RealLoad;
ReactLoad = 0.1*ReactLoad;

%% Define NN architecture

layersPMean = [
    featureInputLayer(inputSize,'Name','input')
    tanhLayer('Name','tanh1')
    fullyConnectedLayer(10, 'Name','fc1')
    tanhLayer('Name','tanh2')
    fullyConnectedLayer(outputSize, 'Name','output')];
meanNet = dlnetwork(layersPMean);

layersPVar = [
    featureInputLayer(inputSize,'Name','input')
    tanhLayer('Name','tanh1')
    fullyConnectedLayer(10, 'Name','fc1')
    tanhLayer('Name','tanh2')
    fullyConnectedLayer(outputSize, 'Name','output')];
varNet = dlnetwork(layersPVar);

%% Extract NN parameters from architecture

ngroupsMean = numel(meanNet.Learnables)/3;
ngroupsVar = numel(varNet.Learnables)/3;

paramMean = [];
paramVar = [];

meanNetParams = numel(paramMean);
varNetParams = numel(paramVar);

ppool = gcp;

RealDemand = RealLoad(loadbus);
ReactDemand = ReactLoad(loadbus);

%% define initial state

state1 = InitSoC*ones(1,nESS);
state1 = [state1,FuelTotal*ones(1,nMT)];
for h = 1:H
    state1 = [state1,RealLoad(loadbus)'];
end
for h = 1:H
    state1 = [state1,ReactLoad(loadbus)'];
end
for h = 1:H
    state1 = [state1,min(normrnd(meanPower,varPower,[1,nRES]),InverterNameplate)];
end

%% start episode and timestep iteration

for ep = 1:nEpisodes
    for t = 1:lenEpisode
        %% Extract current parameter values
        paramMean = []; paramVar = [];
        for i = 1:ngroupsMean
            paramMean = [paramMean;vec(double(extractdata(meanNet.Learnables.Value{i})))];
        end
        for i = 1:ngroupsVar
            paramVar = [paramVar;vec(double(extractdata(varNet.Learnables.Value{i})))];
        end
        MeanAvgCollector = zeros(size(paramMean));
        VarAvgCollector = zeros(size(paramVar));
        if t == 1
            curState = dlarray(state1,'C');
        else
            curState = dlarray(state_next,'C');
        end
        %% get gradients
        %
        for i = 1:sz_mean
            [~,meanGrad{i}] = dlfeval(@model,meanNet,curState,i);
        end
        for i = 1:sz_var
            [~,varGrad{i}] = dlfeval(@model,varNet,curState,i);
        end
        meanJac = [];
        varJac = [];
        % extract gradients with BACKPROPAGATION
        for i = 1:sz_mean
            ngroups = numel(meanGrad{i})/3;
            meanGradVec = [];
            for j = 1:ngroups
                meanGradVec = [meanGradVec,vec(double(extractdata(meanGrad{i}.Value{j})))'];
            end
            meanJac = [meanJac;meanGradVec];
        end
        for i = 1:sz_var
            ngroups = numel(varGrad{i})/3;
            varGradVec = [];
            for j = 1:ngroups
                varGradVec = [varGradVec,vec(double(extractdata(meanGrad{i}.Value{j})))'];
            end
            varJac = [varJac;varGradVec];
        end
        %% make noise (no literally)
        at = zeros(1,numel(paramVar)+numel(paramMean));
        Bt = zeros(4*nload+4*nRES+4*nMT+6*nESS,numel(paramVar)+numel(paramMean));
        ct = zeros(4*nload+4*nRES+4*nMT+6*nESS,1);
        dnMean = extractdata(forward(meanNet,curState));
        dnVar = extractdata(forward(varNet,curState));
        for i = 1:MonteCarloParam
            eps = mvnrnd(zeros(sz_var,1),eye(sz_var)); % ideally must include kronecker product
            curAct = diag(dnVar.^2)*eps'+dnMean;
            [curRgrad_a,curCgrad_B,cur_ct] = CalculateLinearParams(curAct,extractdata(curState));
            at = at + (1/MonteCarloParam)*curRgrad_a*[diag(eps)*varJac,meanJac];
            Bt = Bt + (1/MonteCarloParam)*curCgrad_B*[diag(eps)*varJac,meanJac];
            ct = ct + (1/MonteCarloParam)*cur_ct;
        end
        %% solve with CVX
        [sz1,sz2] = size(Bt);
        thetaSize = sz2;
        % sort out single and double conversions
        thetaCur = double([paramMean;paramVar]);
        Bt = double(Bt);
        ct = double(ct);
        cvx_begin quiet
        variable theta(thetaSize)
        maximize at*theta
        Bt*(theta-thetaCur) + ct <= 0
        norm(theta-thetaCur,2) <= 0.2*norm(thetaCur,2)
        cvx_end
        paramMeanNew = theta(1:numel(theta)/2);
        paramVarNew = theta(numel(theta)/2+1:end);
        %% Insert new parameters into NN
        % insert mean values
        cnt = 0;
        for i = 1:numel(meanNet.Learnables)/3
            sz = size(meanNet.Learnables.Value{i});
            nm = sz(1)*sz(2);
            paramExtract = paramMeanNew(cnt+1:cnt+nm);
            paramInsert = dlarray(reshape(paramExtract,[sz(1),sz(2)]));
            meanNet.Learnables.Value{i} = paramInsert;
            cnt = cnt+nm;
        end
        % insert variance values
        cnt = 0;
        for i = 1:numel(varNet.Learnables)/3
            sz = size(varNet.Learnables.Value{i});
            nm = sz(1)*sz(2);
            paramExtract = paramVarNew(cnt+1:cnt+nm);
            paramInsert = dlarray(reshape(paramExtract,[sz(1),sz(2)]));
            varNet.Learnables.Value{i} = paramInsert;
            cnt = cnt+nm ;
        end
        %% solved optimization problem
        episodeInvalid = 0;
        for k = 1:avEvent
            validThisRun = 0;
            s1 = dlarray(curState,'C');
            dnMean = extractdata(forward(meanNet,s1));
            dnVar = extractdata(forward(varNet,s1));
            curSoC = state1(1:nESS);
            curFuel = state1(nESS+1:nESS+nMT);
            res = min(max(normrnd(meanPower,varPower,[nRES,H]),0),InverterNameplate);
            act = diag(dnVar.^2)*mvnrnd(zeros(outputSize,1),eye(outputSize))'+dnMean;
            [~,~,~,newSOC,newFUEL,cvx_optval] = stateSim(act,curSoC,curFuel,RealDemand,ReactDemand,res);
            if isnan(cvx_optval) || isinf(cvx_optval)
                disp("Model did not solve :(");
                % continue;
                state_next = extractdata(curState);
                state_next(SOCIndex) = max(0.9*state_next(SOCIndex),MinSoC);
                state_next(FuelIndex) = max(0.9*state_next(FuelIndex),0);
                randomPowerVar1 = 0.75 + rand*0.25;
                randomPowerVar2 = 0.75 + rand*0.25;
                state_next(RealD) = randomPowerVar1*state_next(RealD);
                state_next(ReactD) = randomPowerVar2*state_next(ReactD);
            else
                disp("Model Solved!!");
                disp(cvx_optval);
                disp(newSOC)
                continue;
            end
        end
    end
end

delete(ppool);

function [f,g] = model(net,dlX,T)
% Calculate objective using supported functions for dlarray
y = forward(net,dlX);
f = y(T); % crossentropy or similar
g = dlgradient(f,net.Learnables,'RetainData',true); % Automatic gradient
end