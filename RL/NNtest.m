clear all;
close all;

numFeatures = 200;
numClasses1 = 20;
numClasses2 = 30;
deletePoolDisable = 0;
poolDisable = 1;
gpuArrayDisable = 1;

layers = [
    featureInputLayer(numFeatures,'Name','input')
    fullyConnectedLayer(numClasses1, 'Name','fc1')
    reluLayer('Name','rlayer')
    fullyConnectedLayer(numClasses2, 'Name','fc2')];
dlnet = dlnetwork(layers);

rng(3);
xSample = rand(1,numFeatures);
if gpuArrayDisable
    dlX = dlarray(xSample,'C');
else
    dlX = dlarray(gpuArray(xSample),'C');
end

f = cell(numClasses2,1);
g = cell(numClasses2,1);

ppool = gcp('nocreate');
if isempty(ppool) && ~poolDisable
    p = parpool;
end

tic;
for i = 1:numClasses2
    [f{i},g{i}] = dlfeval(@model,dlnet,dlX,i);
end
l = toc;

if ~deletePoolDisable && ~poolDisable
    delete(p);
end

param = [];
param2 = [];
Jac = [];

ngroups = numel(dlnet.Learnables)/3;

for i = 1:ngroups
    param = [param;vec(double(extractdata(dlnet.Learnables.Value{i})))];
end

for i = 1:numel(f)
    ngroups = numel(g{i})/3;
    GradVec = [];
    for j = 1:ngroups
        GradVec = [GradVec,vec(double(extractdata(g{i}.Value{j})))'];
    end
    Jac = [Jac;GradVec];
end

% try to reconstruct the parameters of NN now

% make some changes in the parameter values
param = param + normrnd(0,1,size(param));

% insert values
cnt = 0;
for i = 1:numel(dlnet.Learnables)/3
    sz = size(dlnet.Learnables.Value{i});
    nm = sz(1)*sz(2);
    paramExtract = param(cnt+1:cnt+nm);
    paramInsert = dlarray(reshape(paramExtract,[sz(1),sz(2)]));
    dlnet.Learnables.Value{i} = paramInsert;
end

for i = 1:ngroups
    param2 = [param2;vec(double(extractdata(dlnet.Learnables.Value{i})))];
end

function [f,g] = model(net,dlX,T)
% Calculate objective using supported functions for dlarray
y = forward(net,dlX);
f = y(T); % crossentropy or similar
g = dlgradient(f,net.Learnables,'RetainData',true); % Automatic gradient
end


