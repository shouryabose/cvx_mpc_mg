cvx_begin
    variable x nonnegative;
    variable y nonnegative;
    minimize 1
    norm([x,y],2)<=1
cvx_end