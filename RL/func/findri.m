function rowindex = findri(X,a,b)

% find which row of 2 column matrix X has row value [a,b]
% else return empty variable

[sz1,sz2] = size(X);

assert(sz2==2,'Matrix X doesnt have 2 columns.');

rowindex = [];

for i = 1:sz1
    if X(i,:) == [a,b]
        rowindex = [rowindex;i];
    end
end

end