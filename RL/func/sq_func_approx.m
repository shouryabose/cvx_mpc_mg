function [M,C] = sq_func_approx(nsegments,wsegment)

assert(mod(nsegments,2)==0,'Number of segments should be even');
n = nsegments/2;

xpos = 0:wsegment:n*wsegment;
xneg = -xpos(end:-1:2);
x = [xneg,xpos];
y = x.^2;

M = zeros(numel(x)-1,1);
C = M;

for i = 1:numel(x) - 1
    M(i) = (y(i+1)-y(i))/(x(i+1)-x(i));
    C(i) = (y(i)*x(i+1)-y(i+1)*x(i))/(x(i+1)-x(i));
end

end
