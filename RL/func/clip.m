function C = clip(X,lb,ub)

% clip X such that lb <= X <= ub

% convert everything to double just to be safe
X = double(X);
lb = double(lb);
ub = double(ub);

C = min(max(X,lb),ub);

end