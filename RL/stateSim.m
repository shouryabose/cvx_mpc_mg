function [Pbus Qbus V2bus SOC Fuel cvx_optval] = stateSim(act,SOC1,Fuel1,RealDemand,ReactDemand,RESPow)

addpath('func');

global H loadbus MTbus ESSbus RESbus;
global RBUS XBUS BusBranch;
global M C;
global Pload Qload PMT QMT PRES QRES PESSCh PESSDis QESS sz_mean sz_var;
global efficiencyFactor MTUpperRealLimit MTLowerRealLimit MTUpperReactLimit MTLowerReactLimit FuelTotal;
global meanPower InverterNameplate;
global MaxChargeRealPower MaxDischargeRealPower dischargeEfficiency chargeEfficiency MaxSoC MinSoC MaxESSReactPower MinESSReactPower;
global Vmin Vmax;

nload = numel(loadbus);
nMT = numel(MTbus);
nRES = numel(RESbus);
nESS = numel(ESSbus);
nbus = nload+nMT+nRES+nESS;
[sz1,sz2] = size(BusBranch);
nbranch = sz1;

% assert(size(RESPow) == [nRES H],'Size mismatch of RES real power prediction.');

% here we solve a feasibility problem that involves finding out nodal
% current, voltages, SOC values and Actions for the

cvx_begin quiet
variable Pbus(H*nbus);
variable Qbus(H*nbus);
variable V2bus(H*nbus) nonnegative;
variable Pline(H*nbranch);
variable Qline(H*nbranch);
variable I2line(H*nbranch);
variable Pch(H*nESS);
variable Qch(H*nESS);
variable Pdis(H*nESS);
variable SOC(H*nESS);
variable Fuel(H*nMT);
variable PEpVar(H*nbranch);
variable QEpVar(H*nbranch);
minimize 1 % trivial objective function
% voltage
Vmin <= V2bus <= Vmax;
for t = 1:H
    mbus = (t-1)*nbus;
    mbranch = (t-1)*nbranch;
    mESS = (t-1)*nESS;
    mMT = (t-1)*nMT;
    mRES = (t-1)*nRES;
    mload = (t-1)*nload;
    % DistFlow first set
    for c1 = 1:nbus
        nbd_downstream = findri2(BusBranch,c1); % Downstream neighborhood of node c1
        nbd_upstream = findri3(BusBranch,c1); % Upstream neighborhood of node c1
        res_u = RBUS(nbd_upstream);
        reac_u = XBUS(nbd_upstream);
        Pbus(mbus+c1) == sum(Pline(mbranch+(nbd_downstream))) - sum(Pline(mbranch+(nbd_upstream))-res_u.*I2line(mbranch+(nbd_upstream)));
        Qbus(mbus+c1) == sum(Qline(mbranch+(nbd_downstream))) - sum(Qline(mbranch+(nbd_upstream))-reac_u.*I2line(mbranch+(nbd_upstream)));
    end
    % DistFlow second set + inequalities
    for c1 = 1:nbranch
        c_br = BusBranch(c1,:);
        res_br = RBUS(c1);
        reac_br = XBUS(c1);
        V2bus(mbus+c_br(2)) == V2bus(mbus+c_br(1)) - 2*(res_br*Pline(mbranch+c1)+reac_br*Qline(mbranch+c1)) + (res_br^2+reac_br^2)*I2line(mbranch+c1);
        I2line(mbranch+c1) == PEpVar(mbranch+c1) + QEpVar(mbranch+c1);
        for c2 = 1:numel(M)
            PEpVar(mbranch+c1) >= M(c2)*Pline(mbranch+c1) + C(c2);
            QEpVar(mbranch+c1) >= M(c2)*Qline(mbranch+c1) + C(c2);
        end
    end
    % real and reactive demand on load buses
    for c1 = 1:nload
        Pbus(mbus+loadbus(c1)) == clip(Pload(mload+c1),-RealDemand(c1),0);
        Qbus(mbus+loadbus(c1)) == clip(Qload(mload+c1),-RealDemand(c1),0);
    end
    % real and reactive generation in MT buses
    for c1 = 1:nMT
        Pbus(mbus+MTbus(c1)) == clip(act(PMT(mMT+c1)),MTLowerRealLimit,MTUpperRealLimit);
        Qbus(mbus+MTbus(c1)) == clip(act(QMT(mMT+c1)),MTLowerReactLimit,MTUpperReactLimit);
    end
    % real and reactive generation in RES buses
    for c1 = 1:nRES
        Pbus(mbus+RESbus(c1)) == clip(act(PRES(mMT+c1)),0,InverterNameplate);
        Qbus(mbus+RESbus(c1)) == clip(act(QRES(mMT+c1)),-(InverterNameplate-meanPower),(InverterNameplate-meanPower));
    end
    % charge and discharge ESS
    for c1 = 1:nESS
        Pch(mESS+c1) == clip(act(PESSCh(mESS+c1)),0,MaxChargeRealPower);
        Pdis(mESS+c1) == clip(act(PESSDis(mESS+c1)),0,MaxDischargeRealPower);
        Pbus(mbus+c1) == -Pch(mESS+c1) + Pdis(mESS+c1);
        Qbus(mbus+c1) == clip(act(QESS(mESS+c1)),MinESSReactPower,MaxESSReactPower);
    end
    % time dependent constraints
    if t == 1
        % SoC 
        for c1 = 1:nESS
            SOC(mESS+c1) == clip(SOC1(c1),MinSoC,MaxSoC);
        end
        % Fuel
        for c1 = 1:nMT
            Fuel(mMT+c1) == clip(Fuel1(c1),0,FuelTotal);
        end
    else
        % SoC
        for c1 = 1:nESS
            SOC(mESS+c1) == SOC(mESS-nESS+c1) + chargeEfficiency*Pch(mESS-nESS+c1) - dischargeEfficiency*Pdis(mESS-nESS+c1);
            MinSoC <= SOC(mESS+c1) <= MaxSoC;
        end
        % Fuel
        for c1 = 1:nMT
            Fuel(mMT+c1) == Fuel(mMT-nMT+c1) - efficiencyFactor*Pbus(mbus-nbus+MTbus(c1));
            0 <= Fuel(mMT+c1) <= FuelTotal;
        end
    end
end
cvx_end

disp(Pbus);
disp(Qbus);
disp(Pch);
disp(Pdis);
disp(Qch);
disp(M);
disp(C);


end