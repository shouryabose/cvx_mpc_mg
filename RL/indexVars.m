% start indexing variables

nESS = numel(ESSbus);
nRES = numel(RESbus);
nload = numel(loadbus);
nMT = numel(MTbus);

% per time block schematic of action variables
% > Load Real Power
% > Load Reactive Power
% > MT Real Power
% > MT Reactive Power
% > RES Real Power
% > RES Reactive Power
% > ESS Charge Real Power
% > ESS Charge Reactive Power

global Pload Qload PMT QMT PRES QRES PESSCh PESSDis QESS sz_mean sz_var;
global count2 SOCIndex FuelIndex RealD ReactD RESIndex;

count = 1;
Pload = [];
Qload = [];
PMT = [];
QMT = [];
PRES = [];
QRES = [];
PESSCh = [];
PESSDis = [];
QESS = [];
for h = 1:H
    % Load Real Power
    for i = (h-1)*nload+1:(h-1)*nload+nload
        Pload(i) = count;
        count = count+1;
    end
    % Load Reactive Power
    for i = (h-1)*nload+1:(h-1)*nload+nload
        Qload(i) = count;
        count = count+1;
    end
    % MT Real Power
    for i = (h-1)*nMT+1:(h-1)*nMT+nMT
        PMT(i) = count;
        count = count+1;
    end
    % MT Reactive Power
    for i = (h-1)*nMT+1:(h-1)*nMT+nMT
        QMT(i) = count;
        count = count+1;
    end
    % RES Real Power
    for i = (h-1)*nRES+1:(h-1)*nRES+nRES
        PRES(i) = count;
        count = count+1;
    end
    % RES Reactive Power
    for i = (h-1)*nRES+1:(h-1)*nRES+nRES
        QRES(i) = count;
        count = count+1;
    end
    % ESS Charge Real Power
    for i = (h-1)*nESS+1:(h-1)*nRES+nESS
        PESSCh(i) = count;
        count = count+1;
    end
    % ESS Discharge Real Power
    for i = (h-1)*nESS+1:(h-1)*nRES+nESS
        PESSDis(i) = count;
        count = count+1;
    end
    % ESS Reactive Power
    for i = (h-1)*nESS+1:(h-1)*nRES+nESS
        QESS(i) = count;
        count = count+1;
    end    
end

count2 = 1;
SOCIndex = [];
FuelIndex = [];
RealD = [];
ReactD = [];
RESIndex = [];

for i = 1:nESS
        SOCIndex(i) = count2;
        count2 = count2 + 1;
end

for i = 1:nMT
        FuelIndex(i) = count2;
        count2 = count2 + 1;
end

for h = 1:H
    for i = 1:nload
        RealD((h-1)*nload+i) = count2;
        count2 = count2 + 1;
    end
end

for h = 1:H
    for i = 1:nload
        ReactD((h-1)*nload+i) = count2;
        count2 = count2 + 1;
    end
end

for h = 1:H
    for i = 1:nRES
        RESIndex((h-1)*nRES+i) = count2;
        count2 = count2 + 1;
    end
end

count2 = count2 - 1; % restore last increment

Indices{1} = Pload;
Indices{2} = Qload;
Indices{3} = PMT;
Indices{4} = QMT;
Indices{5} = PRES;
Indices{6} = QRES;
Indices{7} = PESSCh;
Indices{8} = PESSDis;
Indices{9} = QESS; 

count = count-1; % undo last increment

sz_mean = count;
% sz_var = 0.5*count*(count-1);
sz_var = sz_mean;
sz_act = count2;

% square function approx
global M C;
addpath('func');
[M,C] = sq_func_approx(10,0.5);