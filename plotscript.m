% stairs script

close all;

Vstairs = zeros(tmax,nbus);
Pickstairs = zeros(tmax,nload);
SOCstairs = zeros(tmax,nESS);
Fuelstairs = zeros(tmax,nMT);
Curtstairs = zeros(tmax,nRES);
RESPowstairs = Curtstairs;
SumPowerstairs = zeros(tmax,2);

xaxis = 1:tmax;
tlim = [1,tmax];

for i=1:tmax
    for j = 1:nbus
        Vstairs(i,j) = Result{i}.V2node(j);
    end
    for j = 1:nload
        Pickstairs(i,j) = Result{i}.Pickup(j);
    end
    for j = 1:nESS
        SOCstairs(i,j) = Result{i}.SOC(j);
    end
    for j = 1:nMT
        Fuelstairs(i,j) = Result{i}.Fuel(j);
    end
    for j = 1:nRES
        Curtstairs(i,j) = Result{i}.Curt(j);
        RESPowstairs(i,j) = Result{i}.Pnode(RESbus(j));
    end
    SumPowerstairs(i,1) = sum(Result{i}.Pnode);
    SumPowerstairs(i,2) = sum(Result{i}.Qnode);
end

F1 = figure;
box on; hold on; xlim(tlim); xlabel('Time');
for k = 1:nbus
    stairs(xaxis,Vstairs(:,k),'linewidth',2.5);
    leg{k} = ['Bus ',num2str(k)];
end
lg = legend(leg,'fontsize',10); leg=[];
lg.BoxFace.ColorType='truecoloralpha';
lg.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');

F2 = figure;
box on; hold on; xlim(tlim); xlabel('Time');
for k = 1:nload
    stairs(xaxis,Pickstairs(:,k),'linewidth',2.5);
    leg{k} = ['Load ',num2str(k),' Pickup'];
end
lg = legend(leg); leg=[];
lg.BoxFace.ColorType='truecoloralpha';
lg.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');

F3 = figure;
box on; hold on; xlim(tlim); xlabel('Time');
for k = 1:nESS
    stairs(xaxis,SOCstairs(:,k),'linewidth',2.5);
    leg{k} = ['ESS ',num2str(k),' SOC'];
end
lg = legend(leg); leg=[]; 
lg.BoxFace.ColorType='truecoloralpha';
lg.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');

F4 = figure;
box on; hold on; xlim(tlim); xlabel('Time');
for k = 1:nMT
    stairs(xaxis,Fuelstairs(:,k),'linewidth',2.5);
    leg{k} = ['MT ',num2str(k),' Fuel'];
end
lg = legend(leg); leg=[];
lg.BoxFace.ColorType='truecoloralpha';
lg.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');

F5 = figure;
box on; hold on; xlim(tlim); xlabel('Time');
for k = 1:nRES
    stairs(xaxis,RESPowstairs(:,k),'linewidth',2.5);
    leg{k} = ['RES ',num2str(k),' Power Gen'];
end
for k = 1:nRES
    stairs(xaxis,Curtstairs(:,k).*RESPowstairs(:,k),'--','linewidth',2);
    leg{nRES+k} = ['RES ',num2str(k),' Power After Curt.'];
end
lg = legend(leg); leg=[]; 
lg.BoxFace.ColorType='truecoloralpha';
lg.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');

F6 = figure;
box on; hold on; xlim(tlim); xlabel('Time');
stairs(xaxis,SumPowerstairs(:,1),'linewidth',2.5);
leg{1} = ['Real Power Balance'];
stairs(xaxis,SumPowerstairs(:,2),'linewidth',2.5);
leg{2} = ['Reactive Power Balance'];
lg = legend(leg); leg=[];
lg.BoxFace.ColorType='truecoloralpha';
lg.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');