clear all;
close all;

addpath('func');

MTRelCost = 0.5;
bus_data_format;

tmax = 10;
twindow = 6;
Result = [];

RealLoad = 25*RealLoad;
ReactLoad = 25*ReactLoad;

for t = 1:tmax
    cvx_begin
        variable Pline(twindow*nbr_dir);
        variable Qline(twindow*nbr_dir);
        variable I2line(twindow*nbr_dir) nonnegative;
        variable V2node(twindow*nbus) nonnegative;
        variable Pnode(twindow*nbus);
        variable Qnode(twindow*nbus);
        variable Pch(twindow*nESS);
        variable Pdis(twindow*nESS);
        variable SOC(twindow*nESS);
        variable Fuel(twindow*nMT);
        variable Curt(twindow*nRES);
        variable MTPowRat(twindow*nMT);
        variable Pickup(twindow*nload);
        variable tmpObj(twindow);
        variable Objective;
        maximize Objective;
        % set bounds
        % voltage
        0.95 <= V2node <= 1.05;
        % charge and discharge power
        0 <= Pch <= MaxChargeRealPower;
        0 <= Pdis <= MaxDischargeRealPower;
        % SOC
        MinSoC <= SOC <= MaxSoC;
        % Fuel
        0 <= Fuel <= FuelTotal;
        % Charge discharge
        0 <= Pch <= MaxChargeRealPower;
        0 <= Pdis <= MaxDischargeRealPower;
        % ratios
        0 <= Curt <= 1;
        0 <= Pickup <= 1;
        0 <= MTPowRat <= 1;
%        %current
%         0 <= I2line <= 10;
        for t_mpc = 1:twindow
            muline = (t_mpc-1)*nbr_dir;
            mnode = (t_mpc-1)*nbus;
            mESS = (t_mpc-1)*nESS;
            mRES = (t_mpc-1)*nRES;
            mload = (t_mpc-1)*nload;
            mMT = (t_mpc-1)*nMT;
            % DistFlow first set
            for c1 = 1:nbus
                nbd_downstream = findri2(BusBranch,c1); % Downstream neighborhood of node c1
                nbd_upstream = findri3(BusBranch,c1); % Upstream neighborhood of node c1
                res_u = RBUS(nbd_upstream);
                reac_u = XBUS(nbd_upstream);
                Pnode(mnode+c1) == sum(Pline(muline+(nbd_downstream))) - sum(Pline(muline+(nbd_upstream))-res_u.*I2line(muline+(nbd_upstream)));
                Qnode(mnode+c1) == sum(Qline(muline+(nbd_downstream))) - sum(Qline(muline+(nbd_upstream))-reac_u.*I2line(muline+(nbd_upstream)));
            end
            % DistFlow second set + inequalities
            for c1 = 1:nbr_dir
                c_br = BusBranch(c1,:);
                res_br = RBUS(c1);
                reac_br = XBUS(c1);
                V2node(mnode+c_br(2)) == V2node(mnode+c_br(1)) - 2*(res_br*Pline(muline+c1)+reac_br*Qline(muline+c1)) + (res_br^2+reac_br^2)*I2line(muline+c1);
                norm([2*Pline(muline+c1),2*Qline(muline+c1),I2line(muline+c1)-V2node(mnode+c_br(1))],2) <= I2line(muline+c1)+V2node(mnode+c_br(1));
            end
            % ESS Constraints
            for c1 = 1:nESS
                Pnode(mnode+ESSbus(c1)) == -Pch(mESS+c1) + Pdis(mESS+c1);
                MinESSReactPower <= Qnode(mnode+ESSbus(c1)) <= MaxESSReactPower;
            end
            % MT Power Bounds
            for c1 = 1:nMT
                Pnode(mnode+MTbus(c1)) == MTPowRat(mMT+c1)*MTUpperRealLimit;
                MTLowerReactLimit <= Qnode(mnode+MTbus(c1)) <= MTUpperReactLimit;
            end
            % RES power
            rng(t_mpc+t^2*t_mpc);
            rRealPower = normrnd(meanPower,varPower);
            RESRealPower = min(max([rRealPower,0]),InverterNameplate);
            for c1 = 1:nRES
                Pnode(mnode+RESbus(c1)) == Curt(mRES+c1)*RESRealPower;
                norm([Pnode(mnode+RESbus(c1)),Qnode(mnode+RESbus(c1))],2) <= InverterNameplate;
            end
            % Load time decoupled constraints
            for c1 = 1:nload
                Pnode(mnode+loadbus(c1)) == -Pickup(mload+c1)*RealLoad(c1);
                Qnode(mnode+loadbus(c1)) == -Pickup(mload+c1)*ReactLoad(c1);
            end
            % time coupled constraints
            if t_mpc == 1
                if t == 1
                    for c1 = 1:nMT
                        Fuel(mMT+c1) == FuelTotal - efficiencyFactor*Pnode(mnode+MTbus(c1));
                        Pnode(mnode+MTbus(c1)) <= RampUp;
                    end
                    for c1 = 1:nESS
                        SOC(mESS+c1) == InitSoC(1,c1) + chargeEfficiency*Pch(mESS+c1) - dischargeEfficiency*Pdis(mESS+c1);
                    end
                else
                    for c1 = 1:nMT
                        Fuel(mMT+c1) == MPCsol.Fuel(c1) - efficiencyFactor*Pnode(mnode+MTbus(c1));
                        RampDown <= Pnode(mnode+MTbus(c1)) - MPCsol.Pnode(MTbus(c1)) <= RampUp;
                    end
                    for c1 = 1:nESS
                        SOC(mESS+c1) == MPCsol.SOC(c1) + chargeEfficiency*Pch(mESS+c1) - dischargeEfficiency*Pdis(mESS+c1);
                    end
                    for c1 = 1:nload
                        Pickup(mload+c1) - MPCsol.Pickup(c1) >= 0;
                    end
                end
            else
                for c1 = 1:nMT
                    Fuel(mMT+c1) == Fuel(mMT-nMT+c1) - efficiencyFactor*Pnode(mnode+MTbus(c1));
                    RampDown <= Pnode(mnode+MTbus(c1)) - Pnode(mnode-nbus+MTbus(c1)) <= RampUp;
                end
                for c1 = 1:nESS
                    SOC(mESS+c1) == SOC(mESS-nESS+c1) + chargeEfficiency*Pch(mESS+c1) - dischargeEfficiency*Pdis(mESS+c1);
                end
                for c1 = 1:nload
                    Pickup(mload+c1) - Pickup(mload-nload+c1) >= 0;
                end
            end
            % Objective function
            tmpObj == sum(Pickup(mload+(1:nload))) - MTRelCost*sum(MTPowRat(mMT+(1:nMT)));
        end
        Objective == sum(tmpObj);
    cvx_end
    MPCsol.Pnode = Pnode(1:nbus);
    MPCsol.Qnode = Qnode(1:nbus);
    MPCsol.V2node = V2node(1:nbus);
    MPCsol.Pline = Pline(1:nbr_dir);
    MPCsol.Qline = Qline(1:nbr_dir);
    MPCsol.Fuel = Fuel(1:nMT);
    MPCsol.Pch = Pch(1:nESS);
    MPCsol.Pdis = Pdis(1:nESS);
    MPCsol.SOC = SOC(1:nESS);
    MPCsol.Pickup = Pickup(1:nload);
    MPCsol.Curt = Curt(1:nRES);
    MPCsol.I2line = I2line(1:nbr_dir);
    Result{t} = MPCsol;
    disp("On iteration "+t);
end

plotscript;